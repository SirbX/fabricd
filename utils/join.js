const Container = require('../lib/rhea/lib/container');
const RPC = require('../lib/rhea/lib/rpc');
const rpc_connect = require('./conn');

var container = new Container();

var url = process.argv[2];
var contact = process.argv[3];

var conn = rpc_connect(container, url)
var client = RPC.client(conn, '$kademlia');
client.call('join', {contact: contact}, (err, res) => {
    if (err)
        console.error(err);
    else
        console.log(res.body);

    client.close();
});
