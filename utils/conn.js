const url = require('url');
const ws = require('ws');
const Connection = require('rhea/lib/connection');

function connect_http(_url, container, cb) {
  const http = _url.protocol == 'https:'? require('https') : require('http');

  var options = {
    host: _url.hostname,
    port: _url.port || (_url.protocol == 'https:'? 443 : 80),
    headers: {
      'Connection': 'upgrade',
      'Upgrade': 'AMQP10',
    },
  }

  http.get(options, (res) => {
    console.log(res.body);
    res.on('data', (d) => {
      process.stdout.write(d);
    });
  }).on('error', (e) => {
    console.error(e);
  }).on('upgrade', (res, socket, upgradeHead) => {
    var conn = new Connection({
      connection_details() {
        return {
          connect(port, host, options, callback) {
            return socket;
          }
        }
      }
    }, container);
    conn.connect().connected();
    cb(null, conn);
  });
}

function rpc_connect(container, _url, cb) {
  var u = url.parse(_url);

  switch (u.protocol) {
    case 'amqp:':
      cb(null, container.connect({'host': u.hostname, 'port': u.port}));
      break;
    case 'http:':
    case 'https:':
      connect_http(u, container, cb);
      break;
    case 'ws:':
    case 'wss:':
      const wsc = container.websocket_connect(ws);
      cb(null, container.connect({
        connection_details: new wsc(u.href, ['binary', 'AMQPWSB10', 'amqp']),
      }));
      break;
    default:
      throw "Unkwon scheme";
  }
}

module.exports = rpc_connect;
