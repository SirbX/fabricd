const Container = require('../lib/rhea/lib/container');
const RPC = require('../lib/rhea/lib/rpc');
const rpc_connect = require('./conn');

var container = new Container();

var url = process.argv[2];
var key = process.argv[3];
var value = process.argv[4];

var conn = rpc_connect(container, url)
var client = RPC.client(conn, '$kademlia');
client.call('store', {key: key, value: value}, (err, res) => {
    if (err)
        console.error(err);
    else
        console.log(res.body);

    client.close();
});
