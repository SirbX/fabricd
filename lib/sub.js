class Subscription {
  constructor(id) {
    this.id = id;
    this.consumers = {};
    this.queue = null;
  }

  attach(consumer) {
    this.consumers[consumer.id] = consumer;
  }

  detach(consumer) {
    if (this.consumers[consumer.id]) {
      delete this.consumers[consumer.id];
    }
  }

  publish(msg) {
    const keys = Object.keys(this.consumers);
    if (keys.length > 0) {
      keys.forEach(c => this.consumers[c].send(msg));
    } else {
      /* TODO: Queue */
    }
  }

  dispatch() {
    /* TODO: dispatch all messages in the queue */
  }
}

class SubscriptionManager {
  constructor(node) {
    this.node = node;
    this.subs = {};
  }

  get(sub) {
    if (!this.subs[sub]) {
      this.subs[sub] = new Subscription(sub);

      /* TODO Publish to KademliaDHT */
    }

    return this.subs[sub];
  }

  subscribe(sub, consumer) {
    this.get(sub).attach(consumer);
  }

  unsubscribe(sub, consumer) {
    if (this.subs[sub]) {
      this.subs[sub].detach(consumer);
    }
  }

  send(sub, msg) {
    this.get(sub).publish(msg);
  }

  dispatch(sub) {
    this.get(sub).dispatch();
  }
}

module.exports = SubscriptionManager;
