const kadence = require('kadence');
const bunyan = require('bunyan');
const levelup = require('levelup');
const encoding = require('encoding-down');
const leveldown = require('leveldown');
const memdown = require('memdown');

class Kademlia {
  constructor(node) {
    //const storage = levelup(encoding(leveldown('./storage/storage.db')));
    const storage = levelup(encoding(memdown()));
    const logger = bunyan.createLogger({name: 'kadence'});
    //const transport = new kadence.HTTPSTransport();
    const transport = new kadence.UDPTransport();
    const identity = kadence.utils.getRandomKeyBuffer();
    const kport = (1500 + Math.random() * 10000).toFixed() - 0;
    const contact = {hostname: 'localhost', port: kport};

    var kademlia = new kadence.KademliaNode({
      transport,
      storage,
      logger,
      identity,
      contact
    });

    kademlia.listen(kport);
    node.kademlia = kademlia;
    console.log({'event': 'kademlia', 'id': identity.toString('hex'), 'contact': kademlia.contact});

    this.node = node;
    this.kademlia = kademlia;
    node.router.addRoute('$kademlia', this.routeHandler.bind(this));
  }

  routeHandler(address, message) {
    console.log(message);
    var reply_to = message.reply_to;
    var consumer = this.node.consumers.get(reply_to);

    switch (message.subject) {
      case 'join':
        var contact_url = message.body.contact;
        var contact = kadence.utils.parseContactURL(contact_url);

        this.kademlia.join(contact, () => {
          consumer.send({
            correlation_id: message.correlation_id,
            body: {'peers': this.kademlia.router.size},
          });
        });
        break;
      case 'store':
        var key = kadence.utils.hash160(message.body.key);
        var value = message.body.value;

        this.kademlia.iterativeStore(key, value, (err, number) => {
          consumer.send({
            correlation_id: message.correlation_id,
            body: number,
          });
        });
        break;
      case 'fetch':
        var key = kadence.utils.hash160(message.body.key);
        this.kademlia.iterativeFindValue(key, (err, entry, contact) => {
          consumer.send({
            correlation_id: message.correlation_id,
            body: entry.hasOwnProperty('value')? entry : null,
          });
        });
        break;
    }
  }
}

module.exports = Kademlia;
