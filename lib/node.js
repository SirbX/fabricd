const Container = require('rhea');

const Consumer = require('./consumer').Consumer;
const ConsumersPool = require('./consumer').ConsumersPool;
const Tags = require('./tags');
const SubscriptionManager = require('./sub');
const ManagementNode = require('./management').ManagementNode;
const Router = require('./router');
const Kademlia = require('./kademlia');

const transport = require('./transport');

class Node {
  constructor() {
    this.container  = new Container();
    this.router     = new Router(this);
    this.management = new ManagementNode(this, 'com.sirbx.fabricd');
    this.tags       = new Tags(this);
    this.consumers  = new ConsumersPool(this);
    this.subs       = new SubscriptionManager(this);
    this.kademlia   = new Kademlia(this);

    this._init();
  }

  _init() {
    var $node = this;

    this.container.on('sender_open', function (ctx) {
      var source = null;
      var target = null;
      var consumer_id = $node.container.generate_uuid();

      /* Get ATTACH frame from remote */
      var attach = ctx.sender.remote.attach;

      if (attach && attach.source) {
        if (attach.source.address) {
          /* Copy source address from remote
           * TODO: Use link redirection when available
           */
          source = attach.source.address;
          ctx.sender.set_source({address: source});
        } else if (attach.source.dynamic) {
          /* Assign a UUID */
          source = consumer_id;
          ctx.sender.set_source({address: source});
        }
      }

      /* Copy target address from remote, if available */
      if (attach && attach.target && attach.target.address) {
        target = attach.target.address;
        ctx.sender.set_target({address: target});
      }

      /* Regsiter consumer */
      var consumer = $node.consumers.register(consumer_id, ctx.sender);

      console.log({"message": "established sender link", "event": "sender_open",
        "source": source, "target": target});

      if (source) {
        $node.subs.subscribe(source, consumer);
      }
    });

    this.container.on('receiver_open', function (ctx) {
      var source = null;
      var target = null;

      /* Get ATTACH frame from remote */
      var attach = ctx.receiver.remote.attach;

      /* Copy target address from remote, if available
       * TODO: Use link redirection when available
       */
      if (attach && attach.target && attach.target.address) {
        target = attach.target.address;
        ctx.receiver.set_target({address: target});
      }

      /* Copy source address from remote, if available */
      if (attach && attach.source && attach.source.address) {
        source = attach.source.address;
        ctx.receiver.set_source({address: source});
      }

      console.log({"message": "established receiver link", "event": "receiver_open",
        "source": source, "target": target})
    });

    this.container.on('message', function (ctx) {
      var message = ctx.message;

      if (!message.to) {
        message.to = ctx.receiver.target.address;
      }

      $node.router.push(message.to, message);
    });

    this.container.on('session_close', function (ctx) {
      console.log('session_close');
      console.log(ctx);
    });

    this.container.on('session_close', function (ctx) {
      console.log('session_close');
      console.log(ctx);
    });

    this.container.on('disconnected', function (ctx) {
      console.log('disconnected');
      /* TODO Handle stale links */
    });
  }

  run(args) {
    new transport.AMQP(this);
    new transport.HTTP(this);
  }
}

module.exports = Node;
