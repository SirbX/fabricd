class ManageableEntity {
  constructor(name, id) {
    this.entityId = id;
    this.entityName = name;
  }
}

class ManagementNode {
  constructor(node, root) {
    this.node = node;
    this.root = root;
    this.types = {};

    node.router.addRoute('$management', this.routeHandler.bind(this));
  }

  routeHandler(address, message) {
    /* Always reply with not-implemented for now */
    var reply_to = message.reply_to;

    if (reply_to && this.node.consumers.get(reply_to)) {
      var consumer = this.node.consumers.get(reply_to);
      consumer.send({
        correlation_id: message.correlation_id,
        application_properties: {'statusCode': 501},
      });
    }
  }

  registerType(type) {
    this.types[type] = {};
  }

  addEntity(type, entity, fq=false) {

  }
}

module.exports = {
  ManageableEntity,
  ManagementNode
};
