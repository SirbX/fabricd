const utils = require('./utils.js');

class Router {
  constructor(node) {
    this.node = node;
    this.routes = {};
    this._rpc_cb = {};
  }

  addRoute(route, handler) {
    this.routes[route] = handler;
  }

  push(address, message) {
    if (this._rpc_cb[message.correlation_id]) {
      const cb = this._rpc_cb[message.correlation_id];
      delete this._rpc_cb[message.correlation_id];
      return cb(null, message);
    }

    if (this.routes[address]) {
      var handler = this.routes[address];
      return handler(address, message);
    }

    return this.node.subs.send(address, message);
  }

  rpc(address, message, cb) {
    if (!message.correlation_id)
      message.correlation_id = utils.uuid4();

    this.push(address, message);

    var id = message.correlation_id;
    this._rpc_cb[id] = cb;
  }
}

module.exports = Router;
