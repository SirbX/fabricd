class Consumer {
  constructor(id, link) {
    this.id = id;
    this.link = link;
  }

  getId() {
    return this.id;
  };

  send(msg) {
    this.link.send(msg);
  };
}

class ConsumersPool {
  constructor(node) {
    this.node      = node;
    this.consumers = {};
  }

  get(id) {
    return this.consumers[id];
  }

  register(id, link) {
    var consumer = new Consumer(id, link);
    /* TODO check if already registered */
    this.consumers[id] = consumer;

    return consumer;
  }

  unregister(id) {
    delete this.consumers[id];
  }
}

module.exports = {
  Consumer,
  ConsumersPool
}
