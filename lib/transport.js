const Connection = require('rhea/lib/connection');
const net = require('net');

class AMQP {
  constructor(node, options={}) {
    options = Object.assign({port: 0}, options);

    var server = net.createServer();

    server.once('listening', function() {
      var address = server.address();
      var host = address.family === 'IPv6' ? '[' + address.address + ']' : address.address;
      console.log({
        "message": 'started listening',
        "event": 'listening',
        'address': 'amqp://' + host + ':' + address.port
      });
    });

    server.on('connection', function (socket) {
      new Connection(options, node.container).accept(socket);
    });

    server.listen(options);
  }
}

class HTTP {
  constructor(node, options = {}) {
    this.node = node;
    var $node = node;

    options = Object.assign({port: 0}, options);

    var server = net.createServer();
    var _http = require('http').createServer();

    _http.on('upgrade', (req, socket, head) => {
      if (req.headers.upgrade === 'AMQP10') {
        console.log('Got upgrade to AMQP');
        socket.write(
          'HTTP/1.1 101 Switching Protocols\r\n' +
          'Upgrade: AMQP10\r\n' +
          'Connection: Upgrade\r\n\r\n'
        );
        new Connection(options, node.container).accept(socket);
      } else if (req.headers.upgrade === 'websocket') {
        console.log('Got upgrade to WebSocket');
         const wss = new (require('ws').Server)({noServer: true});
         wss.handleUpgrade(req, socket, head, function done(ws) {
          node.container.websocket_accept(ws);
        });
      }
    });

    _http.on('request', (req, res) => {
      console.log(req.url);

      if (!req.url.startsWith('/+')) {
        const fs = require('fs');
        const path = require('path');
        var file_path = '.' + req.url;

        if (file_path == './')
          file_path = './index.html';

        var extname = path.extname(path.join('web/', file_path));
        var contentType = 'text/html';
        switch (extname) {
          case '.js':
            contentType = 'text/javascript';
            break;
          case '.css':
            contentType = 'text/css';
            break;
          case '.json':
            contentType = 'application/json';
            break;
          case '.png':
            contentType = 'image/png';
            break;
          case '.jpg':
            contentType = 'image/jpg';
            break;
          case '.wav':
            contentType = 'audio/wav';
            break;
        }

        fs.readFile(path.join('web/', file_path), function(error, content) {
          if (error) {
            if (error.code == 'ENOENT'){
              fs.readFile('./404.html', function(error, content) {
                res.writeHead(200, { 'Content-Type': contentType });
                res.end(content, 'utf-8');
              });
            } else {
              res.writeHead(500);
              res.end('Sorry, check with the site admin for error: '+ error.code + ' ..\n');
              res.end();
            }
          } else {
            res.writeHead(200, { 'Content-Type': contentType });
            res.end(content, 'utf-8');
          }
        });
      } else {
        /* Service */
        var uri = req.url.slice(2);

        /* rebuild address */
        var comp = uri.split('/');
        if (comp.length < 2) {
          res.writeHead(404, {'Content-Type': 'text/html'});
          res.end('Illegal service id ' + uri);
          return;
        }

        var address = comp[0] + '/' + comp[1];
        var _url = uri.slice(address.length);

        $node.router.rpc(address, {
          subject: '$http',
          application_properties: {
            headers: req.headers,
            method: req.method,
            url: _url
          }
        }, (err, reply) => {
          res.writeHead(200, {'Content-Type': 'text/html'});
          res.end(reply.body);
        });
      }
    });

    server.once('listening', function() {
      var address = server.address();
      var host = address.family === 'IPv6' ? '[' + address.address + ']' : address.address;
      console.log({
        "message": 'started listening',
        "event": 'listening',
        'address': 'http://' + host + ':' + address.port
      });

      const bunyan = require('bunyan');
      const logger = bunyan.createLogger({ name: 'diglet-client', level: 'error' });
      const opts = {
        localAddress: address.address,
        localPort: address.port,
        remoteAddress: 'tunnel.sirbx.com',
        remotePort: 8443,
        verboseLogging: false,
        logger, //: console,
        secureLocalConnection: false
      };

      const diglet = require('diglet');
      this.tunnel = new diglet.Tunnel(opts);

      this.tunnel.once('connected', () => {
        console.log({
          "message": 'node exposed',
          "event": 'expose',
          'tunnel': this.tunnel.url,
        });
      });

      //this.tunnel.once('disconnected', callback);
      this.tunnel.open();
    });

    server.listen(options);
    _http.listen(server);

  }
}

module.exports = {
  AMQP,
  HTTP,
}
